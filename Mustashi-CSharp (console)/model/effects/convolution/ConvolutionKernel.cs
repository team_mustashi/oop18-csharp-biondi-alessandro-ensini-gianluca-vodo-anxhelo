﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace mustashi.model.effects.convolution
{
    class ConvolutionKernel
    {
        public int Rows { get; private set; }
        public int Columns { get; private set; }
        public float[] KernelMatrix { get; private set; }
        public ConvolutionKernel(int rows, int columns, float[] matrix, int divider)
        {
            this.Rows = rows;
            this.Columns = columns;
            this.KernelMatrix = matrix.Select(x => x / (divider > 1 ? divider : 1)).ToArray();
        }
    }
}
