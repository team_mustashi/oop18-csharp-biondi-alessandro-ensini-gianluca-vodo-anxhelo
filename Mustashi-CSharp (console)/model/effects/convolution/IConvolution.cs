﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace mustashi.model.effects.convolution
{
    interface IConvolution : IEffect
    {
        ConvolutionKernel ConvolutionKernel { get; set; }

        int EdgeCondition { get; set; }

    }
}
