﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
//using System.Runtime.Serialization;

namespace mustashi.model.effects
{
    internal interface IEffect
    {
        string EffectName { get; set; }
        EffectType EffectType { get; }
        Bitmap Apply(Bitmap source);
    }
}
