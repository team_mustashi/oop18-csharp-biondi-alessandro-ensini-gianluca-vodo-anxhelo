﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;


namespace mustashi.model.effects
{
    class VoidEffect : IEffect
    {
        public string EffectName { get; set; }
        public string ImageFormat { get; set; }
        public int SavedIndex { get; set; }
        public EffectType EffectType => EffectType.Void;

        public VoidEffect() : this("") { }
        public VoidEffect(string format)
        {
            EffectName = "VoidEffect";
            ImageFormat = format;
        }

        public Bitmap Apply(Bitmap source)
        {
            return source;
        }
    }
}
